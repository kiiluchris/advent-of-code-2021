# Advent Of Code 2021

Implementations of the advent of code 2021 challenges

## Requirements

- Rust
- Cargo

## Running the Challenges

All challenges can be run using the `cargo run` subcommand.
Replace 1 in day-1 with the number of any other day up to 30
by the end of the challenge.

```bash
cargo run --bin day-1
```

## Running Tests

To run all tests you can use the `cargo test` subcommand as shown below.

```bash
cargo test --tests
```


## Credits

- Day 5: [EmilOhlsson](https://github.com/EmilOhlsson/advent-of-code/tree/main/rust/2021/05-hydrothermal-adventure)
  Iterator implementation for range to get fixed length  number range
