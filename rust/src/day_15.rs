use std::{
    cmp::Ordering,
    collections::{BinaryHeap, HashMap},
};

use lib::{setup_environment, Neighbours, Position};

setup_environment!(
    solution_1,
    solution_2,
    15,
    "The lowest total risk of any path from top left to bottom right is: {:?}",
    "The lowest total risk of any path from top left to bottom right after increasing dimensions is: {:?}"
);

type Output = usize;
type RiskLevels = Vec<Vec<u32>>;

fn parse_input(input: &str) -> RiskLevels {
    input
        .split("\n")
        .map(|line| line.split("").flat_map(str::parse).collect())
        .collect()
}

fn reconstruct_path(mut came_from: HashMap<Node, Node>, mut current: Node) -> Vec<Node> {
    let mut path = vec![current];
    while let Some(node) = came_from.remove(&current) {
        current = node;
        path.push(node);
    }
    path.reverse();

    return path;
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
struct Node {
    position: Position,
    risk_level: u32,
    goal: Position,
}

impl Ord for Node {
    fn cmp(&self, other: &Self) -> Ordering {
        self.position
            .manhattan_distance(&self.goal)
            .partial_cmp(&other.position.manhattan_distance(&self.goal))
            .unwrap_or(other.risk_level.cmp(&self.risk_level))
    }
}

impl PartialOrd for Node {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

fn a_star(
    risk_levels: RiskLevels,
    start: Node,
    goal: Node,
    heuristic: fn(&Position, &Position) -> f32,
) -> Vec<Node> {
    let mut unvisited = BinaryHeap::from([start]);
    let mut came_from = HashMap::new();
    let mut risk_costs: HashMap<Node, u32> = HashMap::from([(start, 0)]);
    let mut heuristic_costs: HashMap<Node, f32> =
        HashMap::from([(start, heuristic(&start.position, &goal.position))]);

    while let Some(current) = unvisited.pop() {
        if current == goal {
            return reconstruct_path(came_from, current);
        }

        for neighbour in Neighbours::square(current.position, risk_levels.len(), false) {
            let risk_level = risk_levels[neighbour.y][neighbour.x];
            let neighbour_node = Node {
                position: neighbour,
                risk_level,
                goal: goal.position,
            };
            let possible_total_risk_cost = risk_costs[&current] + risk_level;
            if possible_total_risk_cost < *risk_costs.get(&neighbour_node).unwrap_or(&u32::MAX) {
                came_from.insert(neighbour_node, current);
                risk_costs.insert(neighbour_node, possible_total_risk_cost);
                heuristic_costs.insert(
                    neighbour_node,
                    possible_total_risk_cost as f32 + heuristic(&neighbour, &goal.position),
                );

                unvisited.push(neighbour_node);
            }
        }
    }

    Vec::new()
}

fn increase_risk_level_dimensions(risk_levels: RiskLevels) -> RiskLevels {
    let mut risk_levels_extra_columns: RiskLevels = risk_levels.clone();

    for step in 1..5 {
        for (row_idx, row) in risk_levels.iter().enumerate() {
            let row: Vec<u32> = row
                .iter()
                .map(|risk_level| {
                    let new_risk_level = risk_level + step;
                    if new_risk_level > 9 {
                        new_risk_level - 9
                    } else {
                        new_risk_level
                    }
                })
                .collect();
            risk_levels_extra_columns[row_idx].extend(row);
        }
    }
    let mut risk_levels_extra_rows = risk_levels_extra_columns.clone();
    for step in 1..5 {
        for row in &risk_levels_extra_columns {
            let row: Vec<u32> = row
                .iter()
                .map(|risk_level| {
                    let new_risk_level = *risk_level + step as u32;
                    if new_risk_level > 9 {
                        new_risk_level - 9
                    } else {
                        new_risk_level
                    }
                })
                .collect();
            risk_levels_extra_rows.push(row);
        }
    }
    risk_levels_extra_rows
}

fn run(risk_levels: RiskLevels) -> Output {
    let goal_position = risk_levels.len() - 1;
    let goal_position = Position {
        x: goal_position,
        y: goal_position,
    };
    let start_position = Position { x: 0, y: 0 };
    let start = Node {
        position: start_position,
        goal: goal_position,
        risk_level: 0,
    };
    let goal = Node {
        position: goal_position,
        goal: goal_position,
        risk_level: risk_levels[goal_position.y][goal_position.x],
    };
    let path = a_star(risk_levels, start, goal, Position::manhattan_distance);
    let total_path_risk = path.iter().fold(0, |acc, node| acc + node.risk_level);
    total_path_risk as usize
}

pub fn solution_1(input: &str) -> Output {
    let risk_levels = parse_input(input);
    run(risk_levels)
}

pub fn solution_2(input: &str) -> Output {
    let risk_levels = increase_risk_level_dimensions(parse_input(input));
    run(risk_levels)
}

#[cfg(test)]
mod tests_day_15 {
    use super::*;

    const TEST_INPUT_2: &'static str = include_str!("../../golang/data/day-15/input2.sample");

    #[test]
    fn test_grid_dimension_increment() {
        let grid = increase_risk_level_dimensions(parse_input(TEST_INPUT));
        let string = grid
            .into_iter()
            .map(|row| row.into_iter().map(|c| c.to_string()).collect::<String>())
            .collect::<Vec<String>>()
            .join("\n");
        assert_eq!(TEST_INPUT_2, string);
    }

    #[test]
    fn test_solution_1() {
        assert_eq!(solution_1(TEST_INPUT), 40);
    }

    #[test]
    fn test_solution_2() {
        assert_eq!(solution_2(TEST_INPUT), 315);
    }
}
