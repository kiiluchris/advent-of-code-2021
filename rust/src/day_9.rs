use std::collections::HashSet;

use lib::setup_environment;

setup_environment!(
    solution_1,
    solution_2,
    9,
    "The sum of risk of low points is: {:?}",
    "The product of the 3 largest basins is: {:?}"
);

type Output = Point;
type Point = u32;
type Points = Vec<Vec<Point>>;

fn parse_input(input: &str) -> Points {
    input
        .split("\n")
        .map(|line| line.split("").flat_map(str::parse).collect())
        .collect()
}

fn risk_level(height: Point) -> Point {
    height + 1
}

fn neighbour_indices(
    row: usize,
    col: usize,
    row_size: usize,
    col_size: usize,
) -> impl Iterator<Item = (usize, usize)> {
    let row_size = row_size as i16;
    let col_size = col_size as i16;
    let offsets =
        [(-1, 0), (0, -1), (1, 0), (0, 1)]
            .iter()
            .filter_map(move |&(x, y): &(i16, i16)| {
                let (neighbour_x, neighbour_y) = (col as i16 + x, row as i16 + y);
                if neighbour_x < 0
                    || neighbour_x >= col_size
                    || neighbour_y < 0
                    || neighbour_y >= row_size
                {
                    None
                } else {
                    Some((neighbour_x as usize, neighbour_y as usize))
                }
            });
    offsets
}

fn is_low_point(row: usize, col: usize, points: &Points) -> bool {
    let height = points[row][col];
    for (x, y) in neighbour_indices(row, col, points.len(), points[row].len()) {
        if height >= points[y][x] {
            return false;
        }
    }
    true
}

pub fn solution_1(input: &str) -> Output {
    let points = parse_input(input);
    let mut total_risk = 0;
    for row in 0..points.len() {
        for col in 0..points[row].len() {
            if is_low_point(row, col, &points) {
                total_risk += risk_level(points[row][col]);
            }
        }
    }

    total_risk
}

fn basin_size(
    row: usize,
    col: usize,
    points: &Points,
    visited: &mut HashSet<(usize, usize)>,
) -> Output {
    let mut total = 0;
    for (x, y) in neighbour_indices(row, col, points.len(), points[row].len()) {
        if !visited.contains(&(x, y)) && points[y][x] != 9 {
            visited.insert((x, y));
            total += 1 + basin_size(y, x, points, visited);
        }
    }
    total
}

pub fn solution_2(input: &str) -> Output {
    let points = parse_input(input);
    let mut basins: Vec<Output> = (0..points.len())
        .into_iter()
        .flat_map(|row| {
            (0..points[row].len())
                .into_iter()
                .filter_map(|col| {
                    if is_low_point(row, col, &points) {
                        let mut visited = HashSet::new();
                        visited.insert((col, row));
                        Some(1 + basin_size(row, col, &points, &mut visited))
                    } else {
                        None
                    }
                })
                .collect::<Vec<Output>>()
        })
        .collect();
    basins.sort_by(|a, b| b.cmp(a));
    basins.iter().take(3).product()
}

#[cfg(test)]
mod tests_day_9 {
    use super::*;

    #[test]
    fn test_solution_1() {
        assert_eq!(solution_1(TEST_INPUT), 15);
    }

    #[test]
    fn test_solution_2() {
        assert_eq!(solution_2(TEST_INPUT), 1134);
    }
}
