use std::collections::HashMap;
use std::hash::Hash;

lib::setup_environment!(
    solution,
    solution_life_support,
    3,
    "The power consumption is {}",
    "The life support rating is {}"
);

fn count_occurences<T: Eq + Hash>(xs: impl Iterator<Item = T>) -> HashMap<T, u32> {
    xs.fold(HashMap::new(), |mut map, x| {
        let count = map.entry(x).or_insert(0);
        *count += 1;
        map
    })
}

fn diagnostic_value(bits: Vec<u32>) -> u32 {
    (0..bits.len())
        .map(|idx| 2u32.pow(idx as u32))
        .rev()
        .zip(bits)
        .fold(0, |acc, x| acc + x.1 * x.0)
}

fn parse_diagnostics(diagnostics: &str) -> Vec<Vec<u32>> {
    diagnostics
        .split("\n")
        .flat_map(|line| {
            let words: Vec<u32> = line.split("").flat_map(str::parse).collect();
            if words.is_empty() {
                None
            } else {
                Some(words)
            }
        })
        .collect()
}

fn extract_power_generation_values(bits: Vec<Vec<u32>>) -> (Vec<u32>, Vec<u32>) {
    let row_len = bits[0].len();
    let mut gamma_bits: Vec<u32> = Vec::new();
    let mut epsilon_bits: Vec<u32> = Vec::new();
    for idx in 0..row_len {
        let occurences = count_occurences(bits.iter().map(|row| row[idx]));
        gamma_bits.push(
            occurences
                .clone()
                .into_iter()
                .max_by(|a, b| a.1.cmp(&b.1))
                .map(|(k, _)| k)
                .unwrap(),
        );
        epsilon_bits.push(
            occurences
                .into_iter()
                .min_by(|a, b| a.1.cmp(&b.1))
                .map(|(k, _)| k)
                .unwrap(),
        );
    }

    (gamma_bits, epsilon_bits)
}

fn solution(diagnostics: &str) -> u32 {
    let bits: Vec<Vec<u32>> = parse_diagnostics(diagnostics);
    let (gamma_bits, epsilon_bits) = extract_power_generation_values(bits);
    let gamma = diagnostic_value(gamma_bits);
    let epsilon = diagnostic_value(epsilon_bits);
    gamma * epsilon
}

fn compare_life_support_count(a: &(u32, u32), b: &(u32, u32)) -> std::cmp::Ordering {
    match a.1.cmp(&b.1) {
        std::cmp::Ordering::Equal => {
            if a.0 == 1 {
                std::cmp::Ordering::Greater
            } else {
                std::cmp::Ordering::Less
            }
        }
        x => x,
    }
}

fn filter_remaining_bit_values<F>(idx: usize, bits: Vec<Vec<u32>>, aggregator: F) -> Vec<Vec<u32>>
where
    F: Fn(std::collections::hash_map::IntoIter<u32, u32>) -> Option<(u32, u32)>,
{
    if bits.len() > 1 {
        let counts = count_occurences(bits.iter().map(|row| row[idx]));
        let aggregate = aggregator(counts.into_iter()).map(|(k, _)| k).unwrap();
        bits.into_iter()
            .filter(|bits| bits[idx] == aggregate)
            .collect()
    } else {
        bits
    }
}

fn extract_life_support_values(bits: Vec<Vec<u32>>) -> (Vec<u32>, Vec<u32>) {
    let row_len = bits[0].len();
    let mut oxygen_generation_bits = bits;
    let mut co2_scrubber_bits = oxygen_generation_bits.clone();
    for idx in 0..row_len {
        oxygen_generation_bits = filter_remaining_bit_values(idx, oxygen_generation_bits, |iter| {
            iter.max_by(compare_life_support_count)
        });
        co2_scrubber_bits = filter_remaining_bit_values(idx, co2_scrubber_bits, |iter| {
            iter.min_by(compare_life_support_count)
        });
    }

    (
        oxygen_generation_bits[0].clone(),
        co2_scrubber_bits[0].clone(),
    )
}

fn solution_life_support(diagnostics: &str) -> u32 {
    let bits: Vec<Vec<u32>> = parse_diagnostics(diagnostics);
    let (oxygen_generation_bits, co2_scrubber_bits) = extract_life_support_values(bits);
    let oxygen_generation = diagnostic_value(oxygen_generation_bits);
    let co2_scrubber = diagnostic_value(co2_scrubber_bits);
    oxygen_generation * co2_scrubber
}
#[cfg(test)]
mod tests_day_3 {
    use super::*;

    #[test]
    fn test_day_3() {
        assert_eq!(solution(TEST_INPUT), 198);
    }

    #[test]
    fn test_day_3_life_support() {
        assert_eq!(solution_life_support(TEST_INPUT), 230);
    }
}
