use std::io::Write;

use convert_case::{Case, Casing};
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
struct Opts {
    #[structopt(short, long)]
    day: u8,
    #[structopt(short = "n", long)]
    parsed_input_name: String,
    #[structopt(name = "DIRECTORY")]
    path: String,
}

fn main() -> std::io::Result<()> {
    let opts = Opts::from_args();
    let template = include_str!("../templates/day.rs.jinja");
    let template = template.replace("{{ day }}", &opts.day.to_string());
    let template = template.replace("{{ parsed_input_name_pascal }}", &opts.parsed_input_name);
    let template = template.replace(
        "{{ parsed_input_name }}",
        &opts.parsed_input_name.to_case(Case::Snake),
    );
    let src_dir = format!("{}/src", opts.path);
    let data_dir = format!("{}/../golang/data/day-{}", opts.path, opts.day);
    std::fs::create_dir_all(&src_dir)?;
    std::fs::create_dir_all(&data_dir)?;
    std::fs::OpenOptions::new()
        .create(true)
        .append(true)
        .open(format!("{}/input", data_dir))?;
    std::fs::OpenOptions::new()
        .create(true)
        .append(true)
        .open(format!("{}/input.sample", data_dir))?;
    let mut file = std::fs::OpenOptions::new()
        .write(true)
        .create(true)
        .truncate(true)
        .open(format!("{}/day_{}.rs", src_dir, opts.day))?;
    file.write(template.as_bytes())?;
    Ok(())
}
