lib::setup_environment!(
    solution,
    solution_with_aim,
    2,
    "[No Aim]   Final Horizontal Pos * Final Depth = {}",
    "[With Aim] Final Horizontal Pos * Final Depth = {}"
);

enum Direction {
    Forward,
    Up,
    Down,
}

struct Vector {
    direction: Direction,
    magnitude: i32,
}

fn parse_movements(movements: &str) -> Vec<Vector> {
    movements
        .split("\n")
        .flat_map(|movement| {
            let mut movement = movement.splitn(2, " ");
            let direction = match movement.next()? {
                "forward" => Direction::Forward,
                "up" => Direction::Up,
                "down" => Direction::Down,
                _ => return None,
            };
            let magnitude = movement.next()?.parse::<i32>().ok()?;

            Some(Vector {
                direction,
                magnitude,
            })
        })
        .collect()
}

fn solution(movements: &str) -> i32 {
    let (horizontal, vertical) =
        parse_movements(movements)
            .iter()
            .fold((0, 0), |(horizontal, vertical), movement| {
                match movement.direction {
                    Direction::Up => (horizontal, vertical - movement.magnitude),
                    Direction::Down => (horizontal, vertical + movement.magnitude),
                    Direction::Forward => (horizontal + movement.magnitude, vertical),
                }
            });
    horizontal * vertical
}

fn solution_with_aim(movements: &str) -> i32 {
    let (horizontal, vertical, _) = parse_movements(movements).iter().fold(
        (0, 0, 0),
        |(horizontal, vertical, aim), movement| match movement.direction {
            Direction::Up => (horizontal, vertical, aim - movement.magnitude),
            Direction::Down => (horizontal, vertical, aim + movement.magnitude),
            Direction::Forward => (
                horizontal + movement.magnitude,
                vertical + movement.magnitude * aim,
                aim,
            ),
        },
    );
    horizontal * vertical
}

#[cfg(test)]
mod tests_day_2 {
    use super::*;

    #[test]
    fn test_day_2() {
        assert_eq!(solution(TEST_INPUT), 150);
    }

    #[test]
    fn test_day_2_with_aim() {
        assert_eq!(solution_with_aim(TEST_INPUT), 900);
    }
}
