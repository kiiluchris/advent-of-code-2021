use std::collections::HashMap;

use lib::setup_environment;

setup_environment!(
    solution_1,
    solution_2,
    7,
    "The fuel spent to align to median position = {:?}",
    "The fuel spent to align to approximately mean position = {:?}"
);

type Output = i32;
type Position = Output;

fn parse_input(input: &str) -> Vec<Position> {
    input.split(",").flat_map(str::parse).collect()
}
fn group_positions(positions: Vec<Position>) -> HashMap<Position, Position> {
    positions.into_iter().fold(HashMap::new(), |mut map, pos| {
        *map.entry(pos).or_insert(0) += 1;
        map
    })
}

fn find_median_position(positions: &mut [Output]) -> Output {
    let len = positions.len();
    positions.sort();
    let idx = if len % 2 == 0 { len / 2 } else { len / 2 + 1 };
    positions[idx]
}

pub fn solution_1(input: &str) -> Output {
    let mut positions = parse_input(input);
    let median_position = find_median_position(&mut positions[..]);
    let grouped_positions = group_positions(positions);
    grouped_positions.iter().fold(0, |acc, (pos, count)| {
        let steps = (median_position - pos).abs();
        acc + steps * count
    })
}

fn find_max_position(positions: &[Position]) -> Position {
    positions.iter().max().unwrap().to_owned()
}

pub fn solution_2(input: &str) -> Output {
    let positions = parse_input(input);
    let max_position = find_max_position(&positions[..]);
    let grouped_positions = group_positions(positions);
    (0..max_position)
        .map(|mid_point| {
            grouped_positions.iter().fold(0, |acc, (pos, count)| {
                let steps = (mid_point - pos).abs();
                let fuel_burnt: Position = steps * (steps + 1) / 2;
                acc + fuel_burnt * count
            })
        })
        .min()
        .unwrap()
}

#[cfg(test)]
mod tests_day_7 {
    use super::*;

    #[test]
    fn test_parser() {
        let expected = vec![16, 1, 2, 0, 4, 2, 7, 1, 2, 14];
        assert_eq!(expected, parse_input(TEST_INPUT));
    }

    #[test]
    fn test_solution_1() {
        assert_eq!(solution_1(TEST_INPUT), 37);
    }

    #[test]
    fn test_solution_2() {
        assert_eq!(solution_2(TEST_INPUT), 168);
    }
}
