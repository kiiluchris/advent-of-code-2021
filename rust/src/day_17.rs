use std::cmp::Ordering;

use lib::setup_environment;
use regex::Regex;

setup_environment!(
    solution_1,
    solution_2,
    17,
    "The highest possible y position is: {:?}",
    "The number of distinct initial velocity values is: {:?}"
);

type Output = i16;
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
struct TargetArea {
    min_x: i16,
    max_x: i16,
    min_y: i16,
    max_y: i16,
}

impl TargetArea {
    fn in_area(&self, x: i16, y: i16) -> bool {
        x >= self.min_x && x <= self.max_x && y >= self.min_y && y <= self.max_y
    }

    fn past_area(&self, x: i16, y: i16) -> bool {
        x > self.max_x || y < self.min_y
    }
}

fn parse_input(input: &str) -> TargetArea {
    let re = Regex::new(r"x=(-?\d+)..(-?\d+), y=(-?\d+)..(-?\d+)").unwrap();
    let cap = re.captures(input).unwrap();
    TargetArea {
        min_x: cap[1].parse().unwrap(),
        max_x: cap[2].parse().unwrap(),
        min_y: cap[3].parse().unwrap(),
        max_y: cap[4].parse().unwrap(),
    }
}

fn next_y_velocity(step: i16, initial_y_velocity: i16) -> i16 {
    initial_y_velocity - step
}

fn next_x_velocity(step: i16, initial_x_velocity: i16) -> i16 {
    match initial_x_velocity.cmp(&0) {
        Ordering::Equal => 0,
        Ordering::Less => (step + initial_x_velocity).min(0),
        Ordering::Greater => (initial_x_velocity - step).max(0),
    }
}

fn positions(vel_x: i16, vel_y: i16, target_area: TargetArea) -> Option<Vec<(i16, i16)>> {
    let mut step = 0;
    let mut x = 0;
    let mut y = 0;
    let mut result = vec![(0, 0)];
    while !target_area.past_area(x, y) {
        x += next_x_velocity(step, vel_x);
        y += next_y_velocity(step, vel_y);
        step += 1;
        result.push((x, y));
        if target_area.in_area(x, y) {
            return Some(result);
        }
    }
    None
}

fn max_y_in_positions(ps: Vec<(i16, i16)>) -> i16 {
    ps.into_iter().max_by_key(|p| p.1).map(|p| p.1).unwrap()
}

pub fn solution_1(input: &str) -> Output {
    let target_area = parse_input(input);
    let mut max_y = i16::MIN;
    for vel_x in 0..target_area.min_x {
        for vel_y in 0..target_area.min_x {
            if let Some(y) = positions(vel_x, vel_y, target_area).map(|ps| max_y_in_positions(ps)) {
                if max_y < y {
                    max_y = y;
                }
            }
        }
    }
    max_y
}

pub fn solution_2(input: &str) -> usize {
    let target_area = parse_input(input);
    let mut initial_velocities = Vec::new();
    for vel_x in 0..=target_area.max_x {
        for vel_y in target_area.min_y..-target_area.min_y {
            if let Some(_) = positions(vel_x, vel_y, target_area) {
                initial_velocities.push((vel_x, vel_y));
            }
        }
    }
    initial_velocities.len()
}

#[cfg(test)]
mod tests_day_17 {
    use super::*;

    #[test]
    fn test_parser() {
        assert_eq!(
            parse_input(TEST_INPUT),
            TargetArea {
                min_x: 20,
                max_x: 30,
                min_y: -10,
                max_y: -5
            }
        )
    }

    #[test]
    fn test_solution_1() {
        assert_eq!(solution_1(TEST_INPUT), 45);
    }

    #[test]
    fn test_solution_2() {
        assert_eq!(solution_2(TEST_INPUT), 112);
    }
}
