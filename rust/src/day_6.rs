use lib::{count_occurrences, setup_environment};

setup_environment!(
    solution_1_80_days,
    solution_2_256_days,
    6,
    "The number of lanternfish in 80 days is {}",
    "The number of lanternfish in 256 days is {}"
);

fn solution_1_80_days(input: &str) -> usize {
    solution(input, 80)
}

fn solution_2_256_days(input: &str) -> u64 {
    solution_2(input, 256)
}

fn parse_timers(input: &str) -> Vec<u64> {
    input.split(",").flat_map(str::parse).collect()
}

fn solution(input: &str, days: u64) -> usize {
    (0..days)
        .fold(parse_timers(input), |mut timers, _day| {
            for idx in 0..timers.len() {
                if timers[idx] == 0 {
                    timers[idx] = 7;
                    timers.push(8);
                }
                timers[idx] -= 1;
            }
            timers
        })
        .len()
}

fn solution_2(input: &str, days: u64) -> u64 {
    let timers = parse_timers(input);
    let timers = count_occurrences(timers);
    (0..days)
        .fold(timers, |mut timers, _day| {
            let timers_at_0 = timers.remove(&0).unwrap_or(0);
            for idx in 0..=8 {
                *timers.entry(idx).or_insert(0) = *timers.entry(idx + 1).or_insert(0);
            }
            *timers.entry(8).or_insert(0) = timers_at_0;
            *timers.entry(6).or_insert(0) += timers_at_0;
            timers
        })
        .values()
        .sum()
}

#[cfg(test)]
mod tests_day_6 {
    use super::*;

    #[test]
    fn test_day_6_18_days() {
        let days = 18;
        assert_eq!(solution(TEST_INPUT, days), 26);
        assert_eq!(solution_2(TEST_INPUT, days), 26);
    }

    #[test]
    fn test_day_6_80_days() {
        let days = 80;
        assert_eq!(solution(TEST_INPUT, days), 5934);
        assert_eq!(solution_2(TEST_INPUT, days), 5934);
    }

    #[test]
    fn test_day_6_256_days() {
        let days = 256;
        assert_eq!(solution_2(TEST_INPUT, days), 26984457539);
    }
}
