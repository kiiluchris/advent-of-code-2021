use lib::setup_environment;

setup_environment!(
    solution_1,
    solution_2,
    16,
    "The sum of version numbers in the packets is: {:?}",
    "The solution is: {:?}"
);

type Output = u32;

#[derive(Debug, Clone)]
struct Packet {
    version: u32,
    type_id: u8,
    children: SubPacket,
}

impl Packet {
    fn sum_versions(&self) -> Output {
        self.version
            + match &self.children {
                SubPacket::Literal(_) => 0,
                SubPacket::Children(children) => children
                    .iter()
                    .fold(0, |total, child| total + child.sum_versions()),
            }
    }

    fn calculate_operations(&self) -> u64 {
        match (&self.type_id, &self.children) {
            (4, SubPacket::Literal(lit)) => *lit,
            (0, SubPacket::Children(children)) => children
                .iter()
                .fold(0, |total, child| total + child.calculate_operations()),
            (1, SubPacket::Children(children)) => children
                .iter()
                .fold(1, |total, child| total * child.calculate_operations()),
            (2, SubPacket::Children(children)) => children
                .iter()
                .map(|child| child.calculate_operations())
                .min()
                .unwrap(),
            (3, SubPacket::Children(children)) => children
                .iter()
                .map(|child| child.calculate_operations())
                .max()
                .unwrap(),
            (5, SubPacket::Children(children)) => {
                if children[0].calculate_operations() > children[1].calculate_operations() {
                    1
                } else {
                    0
                }
            }
            (6, SubPacket::Children(children)) => {
                if children[0].calculate_operations() < children[1].calculate_operations() {
                    1
                } else {
                    0
                }
            }
            (7, SubPacket::Children(children)) => {
                if children[0].calculate_operations() == children[1].calculate_operations() {
                    1
                } else {
                    0
                }
            }
            _ => unreachable!(),
        }
    }
}

#[derive(Debug, Clone)]
enum SubPacket {
    Literal(u64),
    Children(Vec<Packet>),
}

const VERSION_END: usize = 3;
const TYPE_ID_END: usize = 6;

fn parse_input(input: &str) -> Packet {
    let input = input
        .split("")
        .flat_map(|c| {
            let number = u8::from_str_radix(c, 16).ok()?;
            Some(format!("{:04b}", number))
        })
        .collect::<Vec<String>>()
        .join("");

    parse_packet(&input).0
}

fn parse_literal(input: &str) -> (SubPacket, usize) {
    let increment = 5;
    let mut index = 0;
    let mut values = vec![];
    while &input[index..index + 1] == "1" {
        values.push(&input[index + 1..index + increment]);
        index += increment;
    }
    values.push(&input[index + 1..index + increment]);
    (
        SubPacket::Literal(u64::from_str_radix(&values.join(""), 2).unwrap()),
        index + increment,
    )
}

fn parse_subpackets(input: &str) -> (Vec<Packet>, usize) {
    let mut index = 1;
    let mut subpackets = vec![];
    if &input[0..1] == "0" {
        let subpacket_len = usize::from_str_radix(&input[index..index + 15], 2).unwrap();
        index += 15;
        let mut sum_of_offset = 0;
        while sum_of_offset < subpacket_len {
            let (packet, offset) = parse_packet(&input[index + sum_of_offset..]);
            subpackets.push(packet);
            sum_of_offset += offset;
        }
        (subpackets, index + subpacket_len)
    } else {
        let n_subpackets = usize::from_str_radix(&input[index..index + 11], 2).unwrap();
        index += 11;
        let mut sum_of_offset = 0;
        for _ in 0..n_subpackets {
            let (packet, offset) = parse_packet(&input[index + sum_of_offset..]);
            subpackets.push(packet);
            sum_of_offset += offset;
        }
        (subpackets, index + sum_of_offset)
    }
}

fn parse_packet(input: &str) -> (Packet, usize) {
    let version = u32::from_str_radix(&input[..VERSION_END], 2).unwrap();
    let type_id = u8::from_str_radix(&input[VERSION_END..TYPE_ID_END], 2).unwrap();
    let index = TYPE_ID_END;
    if type_id == 4 {
        let (value, offset) = parse_literal(&input[index..]);
        let packet = Packet {
            version,
            type_id,
            children: value,
        };
        return (packet, index + offset);
    }

    let (children, offset) = parse_subpackets(&input[index..]);
    let packet = Packet {
        version,
        type_id,
        children: SubPacket::Children(children),
    };
    (packet, index + offset)
}

pub fn solution_1(input: &str) -> Output {
    let packet = parse_input(input);
    packet.sum_versions()
}

pub fn solution_2(input: &str) -> u64 {
    let packet = parse_input(input);
    packet.calculate_operations()
}

#[cfg(test)]
mod tests_day_16 {
    use super::*;

    #[test]
    fn test_solution_1() {
        assert_eq!(solution_1("8A004A801A8002F478"), 16);
        assert_eq!(solution_1("620080001611562C8802118E34"), 12);
        assert_eq!(solution_1("C0015000016115A2E0802F182340"), 23);
        assert_eq!(solution_1("A0016C880162017C3686B18A3D4780"), 31);
    }

    #[test]
    fn test_solution_2() {
        assert_eq!(solution_2("C200B40A82"), 3);
        assert_eq!(solution_2("04005AC33890"), 54);
        assert_eq!(solution_2("880086C3E88112"), 7);
        assert_eq!(solution_2("CE00C43D881120"), 9);
        assert_eq!(solution_2("D8005AC2A8F0"), 1);
        assert_eq!(solution_2("F600BC2D8F"), 0);
        assert_eq!(solution_2("9C005AC2F8F0"), 0);
        assert_eq!(solution_2("9C0141080250320F1802104A08"), 1);
    }
}
