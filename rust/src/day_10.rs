use lib::setup_environment;

setup_environment!(
    solution_1,
    solution_2,
    10,
    "The total syntax error score is: {:?}",
    "The middle autocomplete score is: {:?}"
);

type Output = u64;
type Chunks = Vec<Vec<Chunk>>;

#[derive(Debug, Eq, PartialEq, Clone)]
enum ChunkT {
    SquareOpen,
    SquareClose,
    RoundOpen,
    RoundClose,
    AngleOpen,
    AngleClose,
    CurlyOpen,
    CurlyClose,
}

impl ChunkT {
    fn error_score(&self) -> Output {
        match self {
            Self::RoundClose => 3,
            Self::SquareClose => 57,
            Self::CurlyClose => 1197,
            Self::AngleClose => 25137,
            _ => 0,
        }
    }

    fn autocomplete_score(&self) -> Output {
        match self {
            Self::RoundClose => 1,
            Self::SquareClose => 2,
            Self::CurlyClose => 3,
            Self::AngleClose => 4,
            _ => 0,
        }
    }

    fn parse_open(ch: char) -> Option<Self> {
        match ch {
            '(' => Some(Self::RoundOpen),
            '[' => Some(Self::SquareOpen),
            '{' => Some(Self::CurlyOpen),
            '<' => Some(Self::AngleOpen),
            _ => None,
        }
    }
    fn parse_close(ch: char) -> Option<Self> {
        match ch {
            ')' => Some(Self::RoundClose),
            ']' => Some(Self::SquareClose),
            '}' => Some(Self::CurlyClose),
            '>' => Some(Self::AngleClose),
            _ => None,
        }
    }
    fn get_match(&self) -> Self {
        match self {
            Self::RoundOpen => Self::RoundClose,
            Self::RoundClose => Self::RoundOpen,
            Self::SquareOpen => Self::SquareClose,
            Self::SquareClose => Self::SquareOpen,
            Self::CurlyOpen => Self::CurlyClose,
            Self::CurlyClose => Self::CurlyOpen,
            Self::AngleOpen => Self::AngleClose,
            Self::AngleClose => Self::AngleOpen,
        }
    }
    fn matches(&self, other: &Self) -> bool {
        match (self, other) {
            (Self::RoundOpen, Self::RoundClose)
            | (Self::SquareOpen, Self::SquareClose)
            | (Self::CurlyOpen, Self::CurlyClose)
            | (Self::AngleOpen, Self::AngleClose) => true,
            _ => false,
        }
    }
}

#[derive(Debug)]
struct Chunk {
    opening_tag: ChunkT,
    closing_tag: Option<ChunkT>,
    children: Vec<Chunk>,
}

impl Chunk {
    fn parse(index: usize, line: &str) -> Option<(usize, Chunk)> {
        let opening_tag = ChunkT::parse_open(line[index..].chars().next()?)?;
        let mut children = Vec::new();
        let mut idx = index + 1;
        while idx < line.len() {
            let next_char = line[idx..].chars().next();
            if let Some(tag) = ChunkT::parse_close(next_char?) {
                return Some((
                    idx + 1,
                    Chunk {
                        opening_tag,
                        closing_tag: Some(tag),
                        children,
                    },
                ));
            }
            if let Some((next_idx, child)) = Chunk::parse(idx, line) {
                idx = next_idx;
                children.push(child);
            }
        }
        Some((
            idx,
            Chunk {
                opening_tag,
                closing_tag: None,
                children,
            },
        ))
    }

    fn find_first_invalid_tag(&self) -> Option<ChunkT> {
        match &self.closing_tag {
            Some(closing_tag) if !self.opening_tag.matches(&closing_tag) => {
                return Some(closing_tag.clone())
            }
            _ => {
                for child in &self.children {
                    if let Some(invalid_tag) = child.find_first_invalid_tag() {
                        return Some(invalid_tag);
                    }
                }
            }
        }
        None
    }

    fn find_closing_tags(&self) -> Vec<ChunkT> {
        let mut tags: Vec<_> = self
            .children
            .iter()
            .rev()
            .flat_map(|child| child.find_closing_tags())
            .collect();
        if let None = self.closing_tag {
            tags.push(self.opening_tag.get_match());
        }
        tags
    }
}

fn parse_input(input: &str) -> Chunks {
    input
        .split("\n")
        .map(|line| {
            let mut index = 0;
            let mut chunks = Vec::new();
            while index < line.len() {
                if let Some((next_index, chunk)) = Chunk::parse(index, line) {
                    index = next_index;
                    chunks.push(chunk);
                }
            }
            chunks
        })
        .collect()
}

pub fn solution_1(input: &str) -> Output {
    let chunks = parse_input(input);
    chunks
        .into_iter()
        .flat_map(|line_chunks| {
            line_chunks
                .into_iter()
                .flat_map(|chunk| chunk.find_first_invalid_tag())
        })
        .fold(0, |acc, tag| acc + tag.error_score())
}

pub fn solution_2(input: &str) -> Output {
    let chunks = parse_input(input).into_iter().filter(|line_chunks| {
        line_chunks
            .iter()
            .all(|chunk| chunk.find_first_invalid_tag().is_none())
    });
    let mut scores: Vec<Output> = chunks
        .map(|line_chunks| {
            let tags = line_chunks
                .into_iter()
                .map(|chunk| chunk.find_closing_tags())
                .flatten();
            tags.fold(0, |acc, tag| acc * 5 + tag.autocomplete_score())
        })
        .collect();
    scores.sort();
    scores[scores.len() / 2]
}

#[cfg(test)]
mod tests_day_10 {
    use super::*;

    #[test]
    fn test_solution_1() {
        assert_eq!(solution_1(TEST_INPUT), 26397);
    }

    #[test]
    fn test_solution_2() {
        assert_eq!(solution_2(TEST_INPUT), 288957);
    }
}
