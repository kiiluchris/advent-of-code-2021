use std::collections::HashSet;

use lib::setup_environment;

setup_environment!(
    solution_1,
    solution_2,
    11,
    "After 100 steps there are {:?} flashes",
    "The first step in which all octopi flash is: {:?}"
);

const GRID_SIZE: usize = 10;
const MAX_ENERGY: u8 = 9;
type Output = u32;
type EnergyLevels = [[u8; GRID_SIZE]; GRID_SIZE];

fn parse_input(input: &str) -> EnergyLevels {
    let mut grid = [[0; GRID_SIZE]; GRID_SIZE];
    for (row, line) in input.split("\n").enumerate() {
        for (col, x) in line.split("").flat_map(str::parse).enumerate() {
            grid[row][col] = x;
        }
    }
    grid
}

fn neighbours(point: (usize, usize)) -> Vec<(usize, usize)> {
    let (row, col) = point;
    (-1..=1)
        .flat_map(|row_offset| {
            (-1..=1).filter_map(move |col_offset| {
                let y = row as i8 + row_offset;
                let x = col as i8 + col_offset;
                if (row_offset == 0 && col_offset == 0)
                    || x < 0
                    || x >= GRID_SIZE as i8
                    || y < 0
                    || y >= GRID_SIZE as i8
                {
                    return None;
                }
                Some((y as usize, x as usize))
            })
        })
        .collect()
}

fn increase_energy_of_neighbours(
    point: (usize, usize),
    flashed_points: &mut HashSet<(usize, usize)>,
    energy_levels: &mut EnergyLevels,
) {
    flashed_points.insert(point);
    energy_levels[point.0][point.1] = 0;
    for (row, col) in neighbours(point).into_iter() {
        if !flashed_points.contains(&(row, col)) {
            energy_levels[row][col] += 1;
            if energy_levels[row][col] > MAX_ENERGY {
                increase_energy_of_neighbours((row, col), flashed_points, energy_levels);
            }
        }
    }
}

fn increase_all_energy_levels(energy_levels: &mut EnergyLevels) {
    for row in 0..GRID_SIZE {
        for col in 0..GRID_SIZE {
            energy_levels[row][col] += 1;
        }
    }
}

fn flash_for_energy_levels_gt_9(energy_levels: &mut EnergyLevels) -> HashSet<(usize, usize)> {
    let mut flashed_points = HashSet::new();
    for row in 0..GRID_SIZE {
        for col in 0..GRID_SIZE {
            if energy_levels[row][col] > 9 {
                increase_energy_of_neighbours((row, col), &mut flashed_points, energy_levels);
            }
        }
    }
    flashed_points
}

pub fn solution_1(input: &str) -> Output {
    let mut energy_levels = parse_input(input);
    (1..=100).fold(0, |num_of_flashes, _step| {
        increase_all_energy_levels(&mut energy_levels);
        let flashed_points = flash_for_energy_levels_gt_9(&mut energy_levels);

        num_of_flashes + flashed_points.len() as Output
    })
}

pub fn solution_2(input: &str) -> Output {
    let mut energy_levels = parse_input(input);
    (1..)
        .skip_while(|_step| {
            increase_all_energy_levels(&mut energy_levels);
            flash_for_energy_levels_gt_9(&mut energy_levels);
            let points_at_0 = energy_levels
                .iter()
                .flat_map(|row| row.iter().filter(|&col| *col == 0))
                .count();
            points_at_0 != 100
        })
        .next()
        .unwrap()
}

#[cfg(test)]
mod tests_day_11 {
    use super::*;

    #[test]
    fn test_solution_1() {
        assert_eq!(solution_1(TEST_INPUT), 1656);
    }

    #[test]
    fn test_solution_2() {
        assert_eq!(solution_2(TEST_INPUT), 195);
    }
}
