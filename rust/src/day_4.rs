use std::fmt::{Debug, Display};
use std::str::FromStr;

lib::setup_environment!(
    solution,
    solution_lose,
    4,
    "The first winning score is {}",
    "The last winning score is {}"
);

const BOARD_SIZE: usize = 5;

#[derive(Debug, Clone)]
struct Cell<T> {
    value: T,
    marked: bool,
}

type Board<T> = Vec<Vec<Cell<T>>>;

#[derive(Debug)]
struct GameState<T> {
    moves: Vec<T>,
    boards: Vec<Board<T>>,
}

fn parse_board<'a, T, I>(lines: I) -> Option<Board<T>>
where
    T: FromStr,
    I: Iterator<Item = &'a str>,
{
    let board_lines: Board<T> = lines
        .take_while(|line| *line != "")
        .map(|line| {
            line.split(" ")
                .flat_map(str::parse)
                .map(|value| Cell {
                    value,
                    marked: false,
                })
                .collect()
        })
        .collect();

    if board_lines.is_empty() {
        None
    } else {
        Some(board_lines)
    }
}

fn parse_game_state<T>(input: &str) -> Option<GameState<T>>
where
    T: FromStr + Display + Debug,
{
    let mut lines = input.split("\n");
    let moves: Vec<T> = lines.next()?.split(",").flat_map(str::parse).collect();
    lines.next()?;
    let mut boards: Vec<Board<T>> = Vec::new();
    while let Some(board) = parse_board(&mut lines) {
        boards.push(board);
    }

    Some(GameState { moves, boards })
}

fn mark_board<T>(next_move: &T, board: &mut Board<T>)
where
    T: Eq + Copy,
{
    for row in board {
        for cell in row {
            if cell.value == *next_move {
                *cell = Cell {
                    marked: true,
                    ..*cell
                };
            }
        }
    }
}

fn calculate_winning_score(last_number_called: u32, board: Board<u32>) -> u32 {
    let sum_unmarked_numbers: u32 = board
        .iter()
        .map(|row| {
            row.iter().fold(
                0,
                |sum, cell| if !cell.marked { sum + cell.value } else { sum },
            )
        })
        .sum();

    last_number_called * sum_unmarked_numbers
}

fn run_game(mut game: GameState<u32>) -> u32 {
    for next_move in game.moves {
        for board in &mut game.boards {
            mark_board(&next_move, board);
            for i in 0..BOARD_SIZE {
                let row_winner = board[i].iter().all(|cell| cell.marked);
                let col_winner = board
                    .iter()
                    .map(|row| row[i].clone())
                    .all(|cell| cell.marked);
                if row_winner || col_winner {
                    return calculate_winning_score(next_move, board.to_owned());
                }
            }
        }
    }
    0
}

fn solution(input: &str) -> u32 {
    run_game(parse_game_state(input).unwrap())
}

fn solution_lose(input: &str) -> u32 {
    let mut game: GameState<u32> = parse_game_state(input).unwrap();
    for move_idx in 0..game.moves.len() {
        let next_move = game.moves[move_idx];
        game.boards = game
            .boards
            .iter_mut()
            .flat_map(|board| {
                mark_board(&next_move, board);
                for i in 0..BOARD_SIZE {
                    let row_winner = board[i].iter().all(|cell| cell.marked);
                    let col_winner = board
                        .iter()
                        .map(|row| row[i].clone())
                        .all(|cell| cell.marked);
                    if row_winner || col_winner {
                        return None;
                    }
                }
                Some(board.to_owned())
            })
            .collect();
        if game.boards.len() == 1 {
            return run_game(GameState {
                moves: game.moves[move_idx + 1..].to_vec(),
                boards: game.boards,
            });
        }
    }
    0
}

#[cfg(test)]
mod tests_day_4 {
    use super::*;

    #[test]
    fn test_day_4() {
        assert_eq!(solution(TEST_INPUT), 4512);
    }

    #[test]
    fn test_day_4_lose() {
        assert_eq!(solution_lose(TEST_INPUT), 1924);
    }
}
