use std::collections::HashMap;

#[macro_export]
macro_rules! setup_environment {
    ($solution_1: ident, $solution_2: ident, $day: expr, $message1: expr, $message2: expr) => {
        const INPUT: &'static str = include_str!(concat!("../../golang/data/day-", $day, "/input"));
        const TEST_INPUT: &'static str =
            include_str!(concat!("../../golang/data/day-", $day, "/input.sample"));

        pub fn main() {
            println!(concat!("[Solution 1] ", $message1), $solution_1(INPUT));
            println!(concat!("[Solution 2] ", $message2), $solution_2(INPUT));
        }
    };
}

pub fn sliding_window<T: Sized + Clone>(window_size: usize, values: Vec<T>) -> Vec<Vec<T>> {
    let value_len = values.len();
    let mut result = Vec::with_capacity(std::cmp::max(1, value_len));
    for i in 0..=value_len - window_size {
        result.push(values[i..i + window_size].to_vec());
    }
    result
}

#[derive(Debug, Clone)]
pub struct SlidingWindow<T> {
    idx: usize,
    window_size: usize,
    items: Vec<T>,
}

impl<T> SlidingWindow<T> {
    pub fn new(window_size: usize, items: Vec<T>) -> Self {
        Self {
            idx: 0,
            window_size,
            items,
        }
    }
}

impl<T: Sized + Clone> Iterator for SlidingWindow<T> {
    type Item = Vec<T>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.idx <= self.items.len() - self.window_size {
            let result = Some(self.items[self.idx..self.idx + self.window_size].to_vec());
            self.idx += 1;
            return result;
        }
        None
    }
}

pub fn count_occurrences<T: Eq + std::hash::Hash>(xs: Vec<T>) -> HashMap<T, u64> {
    xs.into_iter().fold(HashMap::new(), |mut map, x| {
        *map.entry(x).or_insert(0) += 1;
        map
    })
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Position {
    pub x: usize,
    pub y: usize,
}

impl Ord for Position {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.y.cmp(&other.y).then_with(|| self.x.cmp(&other.x))
    }
}

impl PartialOrd for Position {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl Position {
    pub fn manhattan_distance(&self, target_position: &Position) -> f32 {
        ((target_position.y as i16 - self.y as i16).abs()
            + (target_position.x as i16 - self.x as i16).abs()) as f32
    }

    pub fn euclidian_distance(&self, target_position: &Position) -> f32 {
        (((target_position.y as i16 - self.y as i16).pow(2)
            + (target_position.x as i16 - self.x as i16).pow(2)) as f32)
            .sqrt()
    }
}

#[derive(Debug, Clone)]
pub struct Neighbours {
    position: Position,
    grid_width: usize,
    grid_height: usize,
    offsets: Vec<(i32, i32)>,
}

impl Neighbours {
    pub fn new(position: Position, grid_width: usize, grid_height: usize, diagonals: bool) -> Self {
        let mut offsets = vec![(-1, 0), (0, -1), (1, 0), (0, 1)];
        if diagonals {
            offsets.extend([(-1, -1), (-1, 1), (1, -1), (1, 1)]);
        }
        Self {
            position,
            grid_width,
            grid_height,
            offsets,
        }
    }

    pub fn square(position: Position, grid_size: usize, diagonals: bool) -> Self {
        Self::new(position, grid_size, grid_size, diagonals)
    }
}

impl Iterator for Neighbours {
    type Item = Position;

    fn next(&mut self) -> Option<Self::Item> {
        while let Some((y_off, x_off)) = self.offsets.pop() {
            let y = self.position.y as i32;
            let y = y + y_off;
            let x = self.position.x as i32;
            let x = x + x_off;
            if y < 0 || y >= self.grid_height as i32 || x < 0 || x >= self.grid_width as i32 {
                continue;
            }
            return Some(Position {
                y: y as usize,
                x: x as usize,
            });
        }
        None
    }
}
