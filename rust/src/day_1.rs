lib::setup_environment!(
    solution,
    solution_sliding,
    1,
    "The number of increasing depths is {}",
    "The number of increasing depths in the sliding window of 3 is {}"
);

fn parse_depths(depths: &str) -> Vec<u16> {
    depths.split('\n').flat_map(|s| s.parse::<u16>()).collect()
}

fn solution(depths: &str) -> u16 {
    parse_depths(depths)
        .iter()
        .fold(
            (0, None),
            |(increments, prev_depth), depth| match prev_depth {
                Some(prev_depth) if prev_depth < depth => (1 + increments, Some(depth)),
                _ => (increments, Some(depth)),
            },
        )
        .0
}

fn solution_sliding(depths: &str) -> u16 {
    let depths = parse_depths(depths);
    let depths = lib::sliding_window(3, depths);
    depths
        .iter()
        .map(|group| group.iter().sum())
        .fold(
            (0 as u16, Option::<u16>::None),
            |(increments, prev_depth), depth| match prev_depth {
                Some(prev_depth) if prev_depth < depth => (1 + increments, Some(depth)),
                _ => (increments, Some(depth)),
            },
        )
        .0
}

#[cfg(test)]
mod tests_day_1 {
    use super::*;

    #[test]
    fn test_sliding_window() {
        let target = vec![vec![1, 2, 3], vec![2, 3, 4], vec![3, 4, 5], vec![4, 5, 6]];
        let actual = lib::sliding_window(3, vec![1, 2, 3, 4, 5, 6]);
        assert_eq!(actual, target);
    }

    #[test]
    fn test_day_1() {
        assert_eq!(solution(TEST_INPUT), 7);
    }

    #[test]
    fn test_day_1_sliding() {
        assert_eq!(solution_sliding(TEST_INPUT), 5);
    }
}
