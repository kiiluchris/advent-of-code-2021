use std::collections::HashMap;

use lib::setup_environment;

setup_environment!(
    solution,
    solution_with_diagonals,
    5,
    "There are at least *{}* points where at least two lines overlap",
    "There are at least *{}* points where at least two lines overlap"
);

#[derive(Debug, Eq, PartialEq, Clone, Hash)]
struct Point {
    x: i16,
    y: i16,
}

#[derive(Debug, Eq, PartialEq, Clone)]
struct Line {
    start: Point,
    end: Point,
}

impl Line {
    #[inline(always)]
    fn start_and_end_attr_eq<T, F>(&self, selector: F) -> bool
    where
        F: Fn(&Point) -> T,
        T: Eq,
    {
        selector(&self.start) == selector(&self.end)
    }

    #[inline(always)]
    fn is_horizontal(&self) -> bool {
        self.start_and_end_attr_eq(|p| p.x)
    }
    #[inline(always)]
    fn is_vertical(&self) -> bool {
        self.start_and_end_attr_eq(|p| p.y)
    }
    #[inline(always)]
    fn is_horizontal_or_vertical(&self) -> bool {
        self.is_horizontal() || self.is_vertical()
    }
    #[inline(always)]
    fn number_of_points(&self) -> i16 {
        std::cmp::max(
            (self.end.x - self.start.x).abs(),
            (self.end.y - self.start.y).abs(),
        ) + 1
    }
}

/// Optionally parses a Point from an &str
fn parse_point(point: &str) -> Option<Point> {
    let mut point = point.splitn(2, ",");
    let x = point.next().map(|p| p.parse().ok())??;
    let y = point.next().map(|p| str::parse(p).ok())??;
    Some(Point { x, y })
}

/// Optionally parses a Line from an &str
fn parse_line(line: &str) -> Option<Line> {
    let mut line = line.splitn(2, " -> ");
    let start = line.next().map(parse_point)??;
    let end = line.next().map(parse_point)??;
    Some(Line { start, end })
}

/// Optionally parses a number of lines from an &str
fn parse_lines(lines: &str) -> Option<Vec<Line>> {
    let lines: Vec<Line> = lines
        .split("\n")
        .flat_map(|line| parse_line(line.trim()))
        .collect();
    if lines.is_empty() {
        None
    } else {
        Some(lines)
    }
}

struct Range {
    current: i16,
    steps_left: i16,
    step: i16,
}

impl Range {
    fn new(start: i16, end: i16, steps_left: i16) -> Range {
        Range {
            current: start,
            steps_left,
            step: (end - start).clamp(-1, 1),
        }
    }
}

impl Iterator for Range {
    type Item = i16;

    fn next(&mut self) -> Option<Self::Item> {
        if self.steps_left == 0 {
            None
        } else {
            let result = Some(self.current);
            self.current += self.step;
            self.steps_left -= 1;
            result
        }
    }
}

fn aggregate_intersections(
    lines: Vec<Line>,
    horizontal_and_vertical_only: bool,
) -> HashMap<Point, i16> {
    lines
        .iter()
        .filter(|line| line.is_horizontal_or_vertical() || !horizontal_and_vertical_only)
        .fold(HashMap::new(), |mut points, line| {
            let steps = line.number_of_points();
            let xs = Range::new(line.start.x, line.end.x, steps);
            let ys = Range::new(line.start.y, line.end.y, steps);
            for (x, y) in xs.zip(ys) {
                *points.entry(Point { x, y }).or_insert(0) += 1;
            }
            points
        })
}

fn count_intersections_of_gt_n_lines(n: i16, intersections: HashMap<Point, i16>) -> usize {
    intersections.values().filter(|&c| *c >= n).count()
}

/// Find the number of points where at least two lines overlap
fn solution(input: &str) -> usize {
    let lines = parse_lines(&input).unwrap();
    let intersections = aggregate_intersections(lines.clone(), true);
    count_intersections_of_gt_n_lines(2, intersections)
}

/// Find the number of points where at least two lines overlap
fn solution_with_diagonals(input: &str) -> usize {
    let lines = parse_lines(&input).unwrap();
    let intersections = aggregate_intersections(lines.clone(), false);
    count_intersections_of_gt_n_lines(2, intersections)
}

#[cfg(test)]
mod tests_day_5 {
    use super::*;

    #[test]
    fn test_day_5_parse_point_empty() {
        assert_eq!(parse_point(""), None);
    }

    #[test]
    fn test_day_5_parse_point() {
        let start = Point { x: 0, y: 9 };
        let end = Point { x: 5, y: 9 };
        assert_eq!(parse_point("0,9"), Some(start));
        assert_eq!(parse_point("5,9"), Some(end));
    }

    #[test]
    fn test_day_5_parse_line_empty() {
        assert_eq!(parse_line(""), None);
    }

    #[test]
    fn test_day_5_parse_line() {
        let start = Point { x: 0, y: 9 };
        let end = Point { x: 5, y: 9 };
        assert_eq!(parse_line("0,9 -> 5,9"), Some(Line { start, end }));
    }

    #[test]
    fn test_day_5_parse_lines_empty() {
        assert_eq!(parse_line(""), None);
    }

    #[test]
    fn test_day_5_parse_lines() {
        let start = Point { x: 0, y: 9 };
        let end = Point { x: 5, y: 9 };
        let start2 = Point { x: 8, y: 0 };
        let end2 = Point { x: 0, y: 8 };
        assert_eq!(
            parse_lines("0,9 -> 5,9"),
            Some(vec![Line {
                start: start.clone(),
                end: end.clone()
            }])
        );
        assert_eq!(
            parse_lines("0,9 -> 5,9\n  8,0 -> 0,8 "),
            Some(vec![
                Line { start, end },
                Line {
                    start: start2,
                    end: end2
                }
            ])
        );
    }

    #[test]
    fn test_day_5() {
        assert_eq!(solution(TEST_INPUT), 5);
    }

    #[test]
    fn test_day_5_with_diagonals() {
        assert_eq!(solution_with_diagonals(TEST_INPUT), 12);
    }
}
