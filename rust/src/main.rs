use structopt::StructOpt;
mod day_1;
mod day_10;
mod day_11;
mod day_12;
mod day_13;
mod day_14;
mod day_15;
mod day_16;
mod day_17;
mod day_2;
mod day_3;
mod day_4;
mod day_5;
mod day_6;
mod day_7;
mod day_8;
mod day_9;

#[derive(Debug, StructOpt)]
struct Opts {
    #[structopt(name = "DAY")]
    day: u8,
}

fn main() {
    let opts = Opts::from_args();
    match opts.day {
        1 => day_1::main(),
        2 => day_2::main(),
        3 => day_3::main(),
        4 => day_4::main(),
        5 => day_5::main(),
        6 => day_6::main(),
        7 => day_7::main(),
        8 => day_8::main(),
        9 => day_9::main(),
        10 => day_10::main(),
        11 => day_11::main(),
        12 => day_12::main(),
        13 => day_13::main(),
        14 => day_14::main(),
        15 => day_15::main(),
        16 => day_16::main(),
        17 => day_17::main(),
        _ => {}
    }
}
