use std::collections::{HashMap, HashSet};

use lib::setup_environment;

setup_environment!(
    solution_1,
    solution_2,
    8,
    "The digits 1,4,7 and 8 appear in the output values {:?} times",
    "The sum of the output values is {}"
);

type Output = u32;
type SignalPattern = String;

fn parse_line(line: &str) -> Vec<SignalPattern> {
    line.split(" ")
        .map(|word| {
            let mut chars = word.split("").collect::<Vec<&str>>();
            chars.sort();
            chars.join("")
        })
        .collect()
}

fn parse_input(input: &str) -> Vec<(Vec<SignalPattern>, Vec<SignalPattern>)> {
    input
        .split("\n")
        .flat_map(|line| {
            let mut line = line.splitn(2, " | ");
            let inputs = parse_line(line.next()?);
            let outputs = parse_line(line.next()?);
            Some((inputs, outputs))
        })
        .collect()
}

pub fn solution_1(input: &str) -> Output {
    let signal_patterns = parse_input(input);
    let unique_patterns_lens: HashSet<_> = vec![2, 3, 4, 7].into_iter().collect();
    signal_patterns.iter().fold(0, |sum, (_, outputs)| {
        sum + outputs
            .iter()
            .filter(|pattern| unique_patterns_lens.contains(&pattern.len()))
            .count() as Output
    })
}

pub fn solution_2(input: &str) -> Output {
    let signal_patterns = parse_input(input);
    let known_mappings: Vec<HashMap<String, u8>> = find_known_mappings(&signal_patterns[..]);
    let patterns_with_mappings = signal_patterns.into_iter().zip(known_mappings.into_iter());
    patterns_with_mappings
        .flat_map(|((inputs, outputs), mut known_mappings)| {
            let mut reversed_mappings: HashMap<_, HashSet<_>> = known_mappings
                .clone()
                .into_iter()
                .map(|(k, v)| (v, k.chars().collect()))
                .collect();
            add_mappings_of_len_6(&inputs[..], &mut known_mappings, &mut reversed_mappings);
            add_mappings_of_len_5(&inputs[..], &mut known_mappings, &mut reversed_mappings);
            outputs
                .iter()
                .map(|output| known_mappings[output].to_string())
                .collect::<Vec<_>>()
                .join("")
                .parse::<Output>()
        })
        .sum()
}

fn add_mappings_of_len_5(
    inputs: &[String],
    known_mappings: &mut HashMap<String, u8>,
    reversed_mappings: &mut HashMap<u8, HashSet<char>>,
) {
    let inputs = inputs.iter().filter(|word| word.len() == 5);
    for input in inputs {
        let set = input.chars().collect();
        if reversed_mappings[&6].difference(&set).count() == 1 {
            reversed_mappings.insert(5, set.clone());
            known_mappings.insert(hashset_to_string(&set), 5);
        } else if reversed_mappings[&7].difference(&set).count() == 0 {
            reversed_mappings.insert(3, set.clone());
            known_mappings.insert(hashset_to_string(&set), 3);
        } else {
            reversed_mappings.insert(2, set.clone());
            known_mappings.insert(hashset_to_string(&set), 2);
        }
    }
}

fn add_mappings_of_len_6(
    inputs: &[String],
    known_mappings: &mut HashMap<String, u8>,
    reversed_mappings: &mut HashMap<u8, HashSet<char>>,
) {
    let inputs = inputs.iter().filter(|word| word.len() == 6);
    for input in inputs {
        let set = input.chars().collect();
        if reversed_mappings[&7].difference(&set).count() == 1 {
            reversed_mappings.insert(6, set.clone());
            known_mappings.insert(hashset_to_string(&set), 6);
        } else if reversed_mappings[&4].difference(&set).count() == 0 {
            reversed_mappings.insert(9, set.clone());
            known_mappings.insert(hashset_to_string(&set), 9);
        } else {
            reversed_mappings.insert(0, set.clone());
            known_mappings.insert(hashset_to_string(&set), 0);
        }
    }
}

fn hashset_to_string(set: &HashSet<char>) -> String {
    let mut chars = set.iter().map(|c| c.to_string()).collect::<Vec<_>>();
    chars.sort();
    chars.join("")
}

fn find_known_mappings(signal_patterns: &[(Vec<String>, Vec<String>)]) -> Vec<HashMap<String, u8>> {
    let unique_patterns_lens: HashMap<usize, u8> =
        vec![(2, 1), (3, 7), (4, 4), (7, 8)].into_iter().collect();
    signal_patterns
        .iter()
        .map(|(inputs, _)| {
            inputs
                .iter()
                .flat_map(|word| {
                    let digit = unique_patterns_lens.get(&word.len())?;
                    Some((word.to_owned(), *digit))
                })
                .collect()
        })
        .collect()
}

#[cfg(test)]
mod tests_day_8 {
    use super::*;

    #[test]
    fn test_solution_1() {
        assert_eq!(solution_1(TEST_INPUT), 26);
    }

    #[test]
    fn test_solution_2() {
        assert_eq!(solution_2(TEST_INPUT), 61229);
    }
}
