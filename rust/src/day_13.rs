use std::collections::HashSet;

use lib::setup_environment;
use regex::Regex;

setup_environment!(
    solution_1,
    solution_2,
    13,
    "The number of dots visible after the first iteration is: {:?}",
    "The code to activate the infrared imaging camera is: \n{}"
);

type Output = usize;
#[derive(Debug, Clone, PartialEq, Eq, Hash, PartialOrd)]
struct Point {
    x: i16,
    y: i16,
}

impl Ord for Point {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        match self.y.cmp(&other.y) {
            std::cmp::Ordering::Equal => self.x.cmp(&other.x),
            ord => ord,
        }
    }
}
#[derive(Debug, Clone, Copy)]
enum Axis {
    X,
    Y,
}
#[derive(Debug, Clone, Copy)]
struct Instruction {
    axis: Axis,
    point: i16,
}
type Transparency = (Vec<Point>, Vec<Instruction>);

fn parse_input(input: &str) -> Transparency {
    let mut input = input.splitn(2, "\n\n");
    let points = input
        .next()
        .unwrap()
        .split("\n")
        .flat_map(|line| {
            let mut line = line.splitn(2, ",").flat_map(str::parse);
            let x = line.next()?;
            let y = line.next()?;
            Some(Point { x, y })
        })
        .collect();
    let re = Regex::new(r"(x|y)=(\d+)$").unwrap();
    let instructions = input
        .next()
        .unwrap()
        .split("\n")
        .flat_map(|line| {
            let cap = re.captures(line)?;
            let axis = if cap[1] == *"x" { Axis::X } else { Axis::Y };
            let point = cap[2].parse().ok()?;
            Some(Instruction { axis, point })
        })
        .collect();

    (points, instructions)
}

fn count_non_overlapping_points(points: Vec<Point>) -> usize {
    points.into_iter().collect::<HashSet<_>>().len()
}

fn fold_transparency_points(
    instruction: Instruction,
    max_y: i16,
    max_x: i16,
    points: &mut Vec<Point>,
) -> (i16, i16) {
    let mid_point = instruction.point;
    match instruction.axis {
        Axis::X => {
            for point in points {
                if point.x < mid_point {
                    point.x = mid_point - point.x - 1;
                } else {
                    point.x = point.x - mid_point - 1;
                }
            }
            (max_y, mid_point)
        }
        Axis::Y => {
            for point in points {
                if point.y > mid_point {
                    point.y = (mid_point * 2) - point.y;
                }
            }
            (mid_point, max_x)
        }
    }
}

pub fn solution_1(input: &str) -> Output {
    let (mut points, instructions) = parse_input(input);
    let max_y = points.iter().max_by_key(|p| p.y).unwrap().y;
    let max_x = points.iter().max_by_key(|p| p.x).unwrap().y;
    fold_transparency_points(instructions[0], max_y, max_x, &mut points);
    points.sort();

    count_non_overlapping_points(points)
}

fn points_as_transparency(max_y: i16, max_x: i16, points: &Vec<Point>) -> String {
    let max_x = max_x as usize;
    let max_y = max_y as usize;
    let mut grid = vec![vec![' '; max_x]; max_y];
    for point in points {
        grid[point.y as usize][point.x as usize] = '#';
    }
    grid.into_iter()
        .map(|row| row.into_iter().rev().collect())
        .collect::<Vec<String>>()
        .join("\n")
}

pub fn solution_2(input: &str) -> String {
    let (mut points, instructions) = parse_input(input);
    let mut max_y = points.iter().max_by_key(|p| p.y).unwrap().y;
    let mut max_x = points.iter().max_by_key(|p| p.x).unwrap().y;
    for instruction in instructions.into_iter() {
        let new_max = fold_transparency_points(instruction, max_y, max_x, &mut points);
        max_y = new_max.0;
        max_x = new_max.1;
    }
    points_as_transparency(max_y, max_x, &points)
}

#[cfg(test)]
mod tests_day_13 {
    use super::*;

    #[test]
    fn test_solution_1() {
        assert_eq!(solution_1(TEST_INPUT), 17);
    }

    #[test]
    fn test_solution_2() {
        assert_eq!(
            solution_2(TEST_INPUT),
            vec!["#####", "#   #", "#   #", "#   #", "#####", "     ", "     "].join("\n")
        );
    }
}
