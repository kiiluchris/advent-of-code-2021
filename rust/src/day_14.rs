use std::collections::HashMap;

use lib::{count_occurrences, setup_environment, SlidingWindow};

setup_environment!(
    solution_1,
    solution_2,
    14,
    "The difference of the most and lest common elements after 10 steps is: {:?}",
    "The difference of the most and lest common elements after 40 steps is: {:?}"
);

type Output = u64;
type PolymerTemplate = Vec<char>;
type InsertionRules = HashMap<(char, char), char>;
type Input = (PolymerTemplate, InsertionRules);
const WINDOW_SIZE: usize = 2;

fn parse_input(input: &str) -> Input {
    let mut input = input.split("\n");
    let template = input.next().unwrap().chars().collect();
    input.next();
    let insertion_rules = input
        .flat_map(|line| {
            let mut line = line.splitn(2, " -> ");
            let mut key = line.next()?.chars();
            let value = line.next()?.chars().next()?;
            let start = key.next()?;
            let end = key.next()?;
            Some(((start, end), value))
        })
        .collect();
    (template, insertion_rules)
}

pub fn solution_1(input: &str) -> Output {
    let (mut polymer_template, insertion_rules) = parse_input(input);
    for _step in 0..10 {
        let mut offset = 1;
        for (i, pair) in SlidingWindow::new(WINDOW_SIZE, polymer_template.clone()).enumerate() {
            let pair = (pair[0], pair[1]);
            if let Some(rule_value) = insertion_rules.get(&pair) {
                polymer_template.insert(i + offset, rule_value.to_owned());
                offset += 1;
            }
        }
    }

    let occurrences = count_occurrences(polymer_template);
    let max = occurrences.iter().max_by_key(|&(_, v)| v).unwrap().1;
    let min = occurrences.iter().min_by_key(|&(_, v)| v).unwrap().1;
    max - min
}

pub fn solution_2(input: &str) -> Output {
    let (polymer_template, insertion_rules) = parse_input(input);
    let mut character_occurrences: HashMap<char, u64> =
        polymer_template
            .clone()
            .into_iter()
            .fold(HashMap::new(), |mut map, character| {
                *map.entry(character).or_insert(0) += 1;
                map
            });
    let mut polymer_template_pair_counts: HashMap<(char, char), u64> = SlidingWindow::new(
        WINDOW_SIZE,
        polymer_template,
    )
    .fold(HashMap::new(), |mut map, window| {
        *map.entry((window[0], window[1])).or_insert(0) += 1;
        map
    });
    for _step in 0..40 {
        let mut new_template = HashMap::new();
        for (pair @ (start, end), count) in polymer_template_pair_counts.into_iter() {
            if let Some(rule_value) = insertion_rules.get(&pair) {
                *character_occurrences.entry(*rule_value).or_insert(0) += count;
                *new_template.entry((start, *rule_value)).or_insert(0) += count;
                *new_template.entry((*rule_value, end)).or_insert(0) += count;
            } else {
                new_template.insert(pair, count);
            }
        }
        polymer_template_pair_counts = new_template;
    }
    let max = character_occurrences.values().max().unwrap();
    let min = character_occurrences.values().min().unwrap();
    max - min
}

#[cfg(test)]
mod tests_day_14 {
    use super::*;

    #[test]
    fn test_solution_1() {
        assert_eq!(solution_1(TEST_INPUT), 1588);
    }

    #[test]
    fn test_solution_2() {
        assert_eq!(solution_2(TEST_INPUT), 2188189693529);
    }
}
