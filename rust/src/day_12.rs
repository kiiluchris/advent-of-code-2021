use std::collections::{HashMap, HashSet};

use lib::setup_environment;

setup_environment!(
    solution_1,
    solution_2,
    12,
    "There number of paths that visit small caves at most once is {:?}",
    "There number of paths that visit small caves at most once with a single repetition is {:?}"
);

type Output = usize;
type Graph = HashMap<String, Vec<String>>;

fn parse_input(input: &str) -> Graph {
    input
        .split("\n")
        .flat_map(|line| {
            let mut line = line.splitn(2, "-");
            let start = line.next()?;
            let end = line.next()?;
            Some((start, end))
        })
        .fold(HashMap::new(), |mut map, (start, end)| {
            if end != "start" {
                map.entry(start.to_string())
                    .or_insert(HashSet::new())
                    .insert(end.to_string());
            }
            if start != "start" {
                map.entry(end.to_string())
                    .or_insert(HashSet::new())
                    .insert(start.to_string());
            }
            map
        })
        .into_iter()
        .map(|(k, v)| {
            let mut v: Vec<_> = v.into_iter().collect();
            v.sort();
            (k, v)
        })
        .collect()
}

fn prepend_paths(start: String, paths: Vec<Vec<String>>) -> Vec<Vec<String>> {
    paths
        .into_iter()
        .map(|sub_path| {
            let mut path = vec![start.to_string()];
            path.extend(sub_path);
            path
        })
        .collect()
}

fn find_paths_dfs(
    start: &str,
    graph: &Graph,
    visited: &mut Vec<String>,
    has_been_visited_twice: bool,
) -> Vec<Vec<String>> {
    let destinations = match graph.get(start) {
        None => return vec![vec![]],
        Some(destinations) => destinations.clone(),
    };
    if start.chars().all(char::is_lowercase) {
        visited.push(start.to_string());
    }
    let paths = destinations
        .iter()
        .flat_map(|to| {
            let has_been_visited_twice = if visited.contains(to) {
                if has_been_visited_twice {
                    return vec![vec![]];
                } else {
                    true
                }
            } else {
                has_been_visited_twice
            };
            let graph_paths = find_paths_dfs(to, graph, visited, has_been_visited_twice);
            prepend_paths(to.to_string(), graph_paths)
        })
        .collect();
    if start.chars().all(char::is_lowercase) {
        visited.pop();
    }
    paths
}

pub fn solution_1(input: &str) -> Output {
    let mut graph = parse_input(input);
    graph.remove("end").unwrap();
    let mut visited = Vec::new();
    find_paths_dfs("start", &graph, &mut visited, true)
        .into_iter()
        .filter(|path| &path[path.len() - 1] == "end")
        .count() as Output
}

pub fn solution_2(input: &str) -> Output {
    let mut graph = parse_input(input);
    graph.remove("end").unwrap();
    let mut visited = Vec::new();
    find_paths_dfs("start", &graph, &mut visited, false)
        .into_iter()
        .filter(|path| &path[path.len() - 1] == "end")
        .count() as Output
}

#[cfg(test)]
mod tests_day_12 {
    use super::*;

    const TEST_INPUT_2: &'static str = include_str!("../../golang/data/day-12/input.sample2");
    const TEST_INPUT_3: &'static str = include_str!("../../golang/data/day-12/input.sample3");

    #[test]
    fn test_solution_1() {
        assert_eq!(solution_1(TEST_INPUT), 10);
        assert_eq!(solution_1(TEST_INPUT_2), 19);
        assert_eq!(solution_1(TEST_INPUT_3), 226);
    }

    #[test]
    fn test_solution_2() {
        assert_eq!(solution_2(TEST_INPUT), 36);
        assert_eq!(solution_2(TEST_INPUT_2), 103);
        assert_eq!(solution_2(TEST_INPUT_3), 3509);
    }
}
