package aoc2021lib

import (
  "embed"
  "fmt"
)

//go:embed data
var f embed.FS

func ReadTestInput(day uint) string {
	contents, _ := f.ReadFile(fmt.Sprintf("data/day-%d/input.sample", day))
	return string(contents)
}

func ReadExtraTestInput(day uint, sampleNumber int) string {
	contents, _ := f.ReadFile(fmt.Sprintf("data/day-%d/input.sample%d", day, sampleNumber))
	return string(contents)
}

func ReadInput(day uint) string {
	contents, _ := f.ReadFile(fmt.Sprintf("data/day-%d/input", day))
	return string(contents)
}

func CartesianProductN(a ...[]int) [][]int {
    c := 1
    for _, a := range a {
        c *= len(a)
    }
    if c == 0 {
        return nil
    }
    p := make([][]int, c)
    b := make([]int, c*len(a))
    n := make([]int, len(a))
    s := 0
    for i := range p {
        e := s + len(a)
        pi := b[s:e]
        p[i] = pi
        s = e
        for j, n := range n {
            pi[j] = a[j][n]
        }
        for j := len(n) - 1; j >= 0; j-- {
            n[j]++
            if n[j] < len(a[j]) {
                break
            }
            n[j] = 0
        }
    }
    return p
}
