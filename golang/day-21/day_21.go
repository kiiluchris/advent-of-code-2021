package main

import (
	"fmt"
	"math"
	"strconv"
	"strings"
  "advent-of-code-2021"
)

type Dice struct {
	Value int
}

func NewDice() Dice {
	return Dice{Value: 0}
}

func (d Dice) String() string {
	return fmt.Sprintf("%d", d.Value)
}

func (d *Dice) Next() int {
	result := d.Value + 1
	if result > 100 {
		result = 1
	}
	d.Value = result
	return result
}

type Player struct {
	Position      int
	NumberOfRolls int
	Score         int
	Idx           int
}

func (p Player) String() string {
	return fmt.Sprintf("(pos = %d, rolls = %d, score = %d)", p.Position, p.NumberOfRolls, p.Score)
}

func NewPlayer(position int, idx int) Player {
	return Player{
		Position:      position,
		NumberOfRolls: 0,
		Score:         0,
		Idx:           idx,
	}
}

func (p *Player) PlayRound(dice *Dice) {
	nextPosition := p.Position
	for i := 0; i < 3; i++ {
		roll := dice.Next()
		nextPosition += roll
	}
	nextPosition %= 10
	if nextPosition == 0 {
		nextPosition = 10
	}
	p.Score += nextPosition
	p.Position = nextPosition
	p.NumberOfRolls += 3
}

func (p Player) HasWon() bool {
	return p.Score >= 1000
}

type DiracGameState struct {
	Player1 Player
	Player2 Player
	Dice    Dice
}

func (s DiracGameState) String() string {
	return fmt.Sprintf("{ dice: %v\n, p1: %v\n, p2: %v \n}", s.Dice, s.Player1, s.Player2)
}

func parsePosition(line string) int {
	pos, _ := strconv.Atoi(line[len(line)-1:])
	return pos
}

func NewDiracGameState(input string) DiracGameState {
	lines := strings.Split(input, "\n")
	return DiracGameState{
		Player1: NewPlayer(parsePosition(lines[0]), 0),
		Player2: NewPlayer(parsePosition(lines[1]), 1),
		Dice:    NewDice(),
	}
}

func (s DiracGameState) NumRolls() int {
	return s.Player1.NumberOfRolls + s.Player2.NumberOfRolls
}

func (s DiracGameState) ProductLoserScoreAndNumRolls() int {
	var loser Player
	if s.Player1.HasWon() {
		loser = s.Player2
	} else {
		loser = s.Player1
	}
	return s.NumRolls() * loser.Score
}

func (s DiracGameState) GameOver() bool {
	return s.Player1.HasWon() || s.Player2.HasWon()
}

func (s *DiracGameState) NextState() {
	s.Player1.PlayRound(&s.Dice)
	if s.Player1.HasWon() {
		return
	}
	s.Player2.PlayRound(&s.Dice)
}

func (s *DiracGameState) PlayGame() {
	for !s.GameOver() {
		s.NextState()
	}
}

func possibleQuamtumRolls() [][3]int {
	rolls := make([][3]int, 27)
  for i, roll := range aoc2021lib.CartesianProductN([]int{1, 2, 3}, []int{1, 2, 3}, []int{1, 2, 3}) {
		rolls[i] = [3]int{roll[0], roll[1], roll[2]}
	}
	return rolls
}

func scoreOfRoll(roll [3]int) int {
	return roll[0] + roll[1] + roll[2]
}

func possibleQuantumScoreFrequencies() map[int]int {
	scores := make(map[int]int)
	for _, roll := range possibleQuamtumRolls() {
		scores[scoreOfRoll(roll)] += 1
	}
	return scores
}

func playQuantumGame(p1Pos int, p1Score int, p2Pos int, p2Score int, cache map[string][2]int64) [2]int64 {
	key := fmt.Sprintf("%d->%d,%d->%d", p1Pos, p1Score, p2Pos, p2Score)
	cachedResult, isCached := cache[key]
	if isCached {
		return cachedResult
	} else if p1Score >= 21 {
		return [2]int64{1, 0}
	} else if p2Score >= 21 {
		return [2]int64{0, 1}
	}
	result := [2]int64{0, 0}
	for roll, frequency := range possibleQuantumScoreFrequencies() {
		pos := p1Pos + roll
		pos %= 10
		if pos == 0 {
			pos = 10
		}
		score := p1Score + pos
		p2Result := playQuantumGame(p2Pos, p2Score, pos, score, cache)
		result[0] += p2Result[1] * int64(frequency)
		result[1] += p2Result[0] * int64(frequency)
	}
	cache[key] = result
	return result
}

func main() {
	testInput := aoc2021lib.ReadTestInput(21)
	testState := NewDiracGameState(testInput)
	testState.PlayGame()
	state := NewDiracGameState(aoc2021lib.ReadInput(21))
	state.PlayGame()
	fmt.Printf("[Test] 1: %d\n", testState.ProductLoserScoreAndNumRolls())
	fmt.Printf("[Real] 1: %d\n", state.ProductLoserScoreAndNumRolls())
	testState = NewDiracGameState(testInput)
	outcomes := make(map[string][2]int64)
	testResult := playQuantumGame(testState.Player1.Position, 0, testState.Player2.Position, 0, outcomes)
  fmt.Println("[Test] 2: ", int64(math.Max(float64(testResult[0]), float64(testResult[1]))))
	outcomes = make(map[string][2]int64)
	state = NewDiracGameState(aoc2021lib.ReadInput(21))
	result := playQuantumGame(state.Player1.Position, 0, state.Player2.Position, 0, outcomes)
  fmt.Println("[Real] 2: ", int64(math.Max(float64(result[0]), float64(result[1]))))
}
