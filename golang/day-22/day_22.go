package day22

import (
	"fmt"
	"strconv"
	"strings"
)


const smallInput = `on x=10..12,y=10..12,z=10..12
on x=11..13,y=11..13,z=11..13
off x=9..11,y=9..11,z=9..11
on x=10..10,y=10..10,z=10..10`

type ParsedRange struct {
	Bounds Range
	IsOn   bool
}

type Range struct {
	X Bounds
	Y Bounds
	Z Bounds
}

func (r Range) Bounds() string {
	return fmt.Sprintf("{x = %v, y = %v, z = %v}", r.X, r.Y, r.Z)
}

func (r Range) Volume() int {
	return r.X.Magnitude() * r.Y.Magnitude() * r.Z.Magnitude()
}

func (r Range) Intersection(other Range) *Range {
	intersectionX := r.X.Intersection(other.X)
	if intersectionX == nil {
		return nil
	}
	intersectionY := r.Y.Intersection(other.Y)
	if intersectionY == nil {
		return nil
	}
	intersectionZ := r.Z.Intersection(other.Z)
	if intersectionZ == nil {
		return nil
	}
	intersection := Range{
		X: *intersectionX,
		Y: *intersectionY,
		Z: *intersectionZ,
	}
	return &intersection
}

type Bounds struct {
	Min int
	Max int
}

func (b Bounds) String() string {
	return fmt.Sprintf("(%d,%d)", b.Min, b.Max)
}

func (b Bounds) Magnitude() int {
	return b.Max - b.Min + 1
}

func (b Bounds) InRange(min, max int) bool {
	return b.Min >= min && b.Max <= max
}

func Min(a, b int) int {
	if a < b {
		return a
	} else {
		return b
	}
}

func Max(a, b int) int {
	if a > b {
		return a
	} else {
		return b
	}
}

func (b Bounds) Intersection(other Bounds) *Bounds {
	maxOfMin := Max(b.Min, other.Min)
	minOfMax := Min(b.Max, other.Max)
	if maxOfMin > minOfMax {
		return nil
	}
	return &Bounds{
		Min: maxOfMin,
		Max: minOfMax,
	}
}

func parseBounds(input string) Bounds {
	bounds := strings.Split(strings.Split(input, "=")[1], "..")
	Min, _ := strconv.Atoi(bounds[0])
	Max, _ := strconv.Atoi(bounds[1])
	return Bounds{
		Min,
		Max,
	}
}

func parseLine(line string) ParsedRange {
	splits := strings.SplitN(line, " ", 2)
	state := splits[0]
	isOn := false
	if state == "on" {
		isOn = true
	}
	all_bounds := strings.Split(splits[1], ",")
	x_bounds := parseBounds(all_bounds[0])
	y_bounds := parseBounds(all_bounds[1])
	z_bounds := parseBounds(all_bounds[2])
	return ParsedRange{
		IsOn: isOn,
		Bounds: Range{
			X: x_bounds,
			Y: y_bounds,
			Z: z_bounds,
		},
	}
}

func parseInput(input string) []ParsedRange {
	lines := strings.Split(input, "\n")
	ranges := make([]ParsedRange, len(lines))
	for i, line := range lines {
		ranges[i] = parseLine(line)
	}
	return ranges
}

type Reactor struct {
	state map[Range]int
}

const initialisationMin = -50
const initialisationMax = 50

func cloneMap(src map[Range]int) map[Range]int {
	dest := map[Range]int{}
	for key, value := range src {
		dest[key] = value
	}
	return dest
}

func (r *Reactor) Reboot(instructions []ParsedRange, limitBounds bool) {
	onOffValues := make(map[Range]int)
	for _, instruction := range instructions {
		if limitBounds && !(instruction.Bounds.X.InRange(initialisationMin, initialisationMax) && instruction.Bounds.Y.InRange(initialisationMin, initialisationMax) && instruction.Bounds.Z.InRange(initialisationMin, initialisationMax)) {
			continue
		}
		newOnOffValues := cloneMap(onOffValues)
		bounds := instruction.Bounds
		onState := instruction.IsOn
		for knownBounds, state := range onOffValues {
			if intersection := knownBounds.Intersection(bounds); intersection != nil {
				newOnOffValues[*intersection] -= state
			}
		}
		if onState {
			newOnOffValues[bounds] += 1
		}
		onOffValues = newOnOffValues
	}
	r.state = onOffValues
}

func (r Reactor) CountOnCubes() int {
	total := 0
	for bounds, state := range r.state {
		total += bounds.Volume() * state
	}
	return total
}
