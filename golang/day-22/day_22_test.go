package day22

import (
	aoc2021lib "advent-of-code-2021"
	"reflect"
	"strings"
	"testing"
)


func TestParseInput(t *testing.T) {
	t.Run("Parse line of input", func(t *testing.T) {
		got := parseLine(strings.Split(smallInput, "\n")[0])
		want := ParsedRange{
			Bounds: Range{
				X: Bounds{Min: 10, Max: 12},
				Y: Bounds{Min: 10, Max: 12},
				Z: Bounds{Min: 10, Max: 12},
			},
			IsOn: true,
		}
		if got != want {
			t.Errorf("Invalid line parsing\ngot %v want %v", got, want)
		}
	})
	t.Run("Parse small input", func(t *testing.T) {
		got := parseInput(smallInput)
		want := []ParsedRange{
			ParsedRange{
				Bounds: Range{
					X: Bounds{Min: 10, Max: 12},
					Y: Bounds{Min: 10, Max: 12},
					Z: Bounds{Min: 10, Max: 12},
				},
				IsOn: true,
			},
			ParsedRange{
				Bounds: Range{
					X: Bounds{Min: 11, Max: 13},
					Y: Bounds{Min: 11, Max: 13},
					Z: Bounds{Min: 11, Max: 13},
				},
				IsOn: true,
			},
			ParsedRange{
				Bounds: Range{
					X: Bounds{Min: 9, Max: 11},
					Y: Bounds{Min: 9, Max: 11},
					Z: Bounds{Min: 9, Max: 11},
				},
				IsOn: false,
			},
			ParsedRange{
				Bounds: Range{
					X: Bounds{Min: 10, Max: 10},
					Y: Bounds{Min: 10, Max: 10},
					Z: Bounds{Min: 10, Max: 10},
				},
				IsOn: true,
			},
		}
		if !reflect.DeepEqual(got, want) {
			t.Errorf("Invalid parsed input\ngot %v expected %v", got, want)
		}
	})
}


func assertReactorInitialisation(t testing.TB, limitBounds bool, input string, want int) {
	t.Helper()
	instructions := parseInput(input)
	reactor := Reactor{}
	reactor.Reboot(instructions, limitBounds)
	got := reactor.CountOnCubes()
	if got != want {
		t.Errorf("Initialisation failed\ngot %d cubes on expected %d", got, want)
	}
}

func TestInitialiseReactor(t *testing.T) {
	t.Run("small input", func(t *testing.T) {
		want := 39
		assertReactorInitialisation(t, true, smallInput, want)
	})
	t.Run("test input", func(t *testing.T) {
		input := aoc2021lib.ReadTestInput(22)
		want := 590784
		assertReactorInitialisation(t, true, input, want)
	})
	t.Run("real input", func(t *testing.T) {
		input := aoc2021lib.ReadInput(22)
		want := 546724
		assertReactorInitialisation(t, true, input, want)
	})
	t.Run("test input part 2", func(t *testing.T) {
		input := aoc2021lib.ReadExtraTestInput(22, 2)
		want := 2758514936282235
		assertReactorInitialisation(t, false, input, want)
	})
	t.Run("real input part 2", func(t *testing.T) {
		input := aoc2021lib.ReadInput(22)
		want := 1346544039176841
		assertReactorInitialisation(t, false, input, want)
	})
}
