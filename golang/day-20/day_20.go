package main

import (
	"fmt"
	"strconv"
	"strings"
  "advent-of-code-2021"
)

type Position struct {
	X int
	Y int
}

func (p Position) String() string {
	return fmt.Sprintf("(%d, %d)", p.X, p.Y)
}

type State struct {
	MinX            int
	MaxX            int
	MinY            int
	MaxY            int
	EnhancementAlgo string
	Image           map[Position]rune
}

func (s State) CountLitPixels() int {
	total := 0
	for _, char := range s.Image {
		if char == '#' {
			total++
		}
	}
	return total
}

func parseInput(input string) State {
	lines := strings.Split(input, "\n")
	enhancement_algo := lines[0]
	image := make(map[Position]rune)
	for row, line := range lines[2:] {
		for col, char := range line {
			pos := Position{X: col, Y: row}
			image[pos] = char
		}
	}
	return State{
		MinX:            0,
		MinY:            0,
		MaxX:            len(lines[2:][0]) - 1,
		MaxY:            len(lines[2:]) - 1,
		Image:           image,
		EnhancementAlgo: enhancement_algo,
	}
}

func printImage(state State) {
	for row := state.MinX; row <= state.MaxX; row++ {
		for col := state.MinY; col <= state.MaxY; col++ {
			currentPos := Position{X: col, Y: row}
			char, isContained := state.Image[currentPos]
			if isContained && char == '#' {
				fmt.Print("#")
			} else {
				fmt.Print(".")
			}
		}
		fmt.Println("")
	}
}

func pixelEnhancementValue(generation int, zeroValue rune, pos Position, image map[Position]rune) int64 {
	characters := make([]rune, 0, 9)
	for row := pos.Y - 1; row <= pos.Y+1; row++ {
		for col := pos.X - 1; col <= pos.X+1; col++ {
			currentPos := Position{X: col, Y: row}
			char, isContained := image[currentPos]
			if !isContained {
				if zeroValue == '.' {
					characters = append(characters, '0')
				} else {
          characters = append(characters, rune("01"[generation % 2]))
				}
			} else if char == '#' {
				characters = append(characters, '1')
			} else {
				characters = append(characters, '0')
			}
		}
	}
	n, _ := strconv.ParseInt(string(characters), 2, 16)
	return n
}

func enhanceImage(generation int, state State) State {
	newImage := make(map[Position]rune)
	newMinX := state.MinX - 1
	newMinY := state.MinY - 1
	newMaxX := state.MaxX + 1
	newMaxY := state.MaxY + 1
	zeroValue := rune(state.EnhancementAlgo[0])
	for row := newMinY; row <= newMaxY; row++ {
		for col := newMinX; col <= newMaxX; col++ {
			pos := Position{X: col, Y: row}
			enhancementIndex := pixelEnhancementValue(generation, zeroValue, pos, state.Image)
			newPixel := state.EnhancementAlgo[enhancementIndex]
			newImage[pos] = rune(newPixel)
		}
	}
	return State{
		Image:           newImage,
		EnhancementAlgo: state.EnhancementAlgo,
		MinX:            newMinX,
		MaxX:            newMaxX,
		MinY:            newMinY,
		MaxY:            newMaxY,
	}
}

func part1(state State, times int) {
	for i := 0; i < times; i++ {
		state = enhanceImage(i, state)
	}
	fmt.Println(state.CountLitPixels())
}

func main() {
	testState := parseInput(aoc2021lib.ReadTestInput(20))
	part1(testState, 0)
	part1(testState, 1)
	part1(testState, 2)
	initialState := parseInput(aoc2021lib.ReadInput(20))
	part1(initialState, 2)
	part1(initialState, 50)
}
