package main

import (
	"fmt"
	"strconv"
	"strings"
  "advent-of-code-2021"
)


type SnailNumber interface {
	isSnailNumber()
}

type Number struct {
	Parent *Pair
	Depth  int
	Value  int
}

func (n *Number) isSnailNumber() {}

func (n *Number) String() string {
	return fmt.Sprintf("%d",  n.Value)
	// return fmt.Sprintf("(%d)%d", n.Depth, n.Value)
}

type Pair struct {
	Parent *Pair
	Depth  int
	Left   SnailNumber
	Right  SnailNumber
}

func (p *Pair) String() string {
	return fmt.Sprintf("[%v, %v]",  p.Left, p.Right)
	// return fmt.Sprintf("(%d)[%v, %v]", p.Depth, p.Left, p.Right)
}

func (p *Pair) isSnailNumber() {}

func (p *Pair) values() []*Number {
	var out []*Number
	switch left := p.Left.(type) {
	case *Pair:
		out = append(out, left.values()...)
	case *Number:
		out = append(out, left)
	}
	switch right := p.Right.(type) {
	case *Pair:
		out = append(out, right.values()...)
	case *Number:
		out = append(out, right)
	}
	return out
}

func incDepth(p SnailNumber) {
	switch v := p.(type) {
	case *Pair:
		v.Depth += 1
		incDepth(v.Left)
		incDepth(v.Right)
	case *Number:
		v.Depth += 1
	default:
		panic("Unknown Type")
	}
}

func (self *Pair) Add(other *Pair) *Pair {
	parent := &Pair{Parent: nil, Depth: 0, Left: self, Right: other}
	self.Parent = parent
	other.Parent = parent
	incDepth(self)
	incDepth(other)
  parent.Reduce()
	return parent
}

func PairSum(pairs []*Pair) *Pair {
  result := pairs[0]
  for _, pair := range pairs[1:] {
    result = result.Add(pair)
  }

  return result
}

func (p *Pair) Magnitude() int {
  total := 0
  switch v := p.Left.(type) {
    case *Pair:
      total += 3 * v.Magnitude()
    case *Number:
      total += 3 * v.Value

  }
  switch v := p.Right.(type) {
    case *Pair:
      total += 2 * v.Magnitude()
    case *Number:
      total += 2 * v.Value

  }
  return total
}

func (p *Pair) Reduce() *Pair {
	modified := true
  steps := 0
	for modified {
    modified = false
		numbers := p.values()
    steps += 1
		for idx, number := range numbers {
			// Explode
			if number.Depth > 4 {
				// Add Left
				if idx > 0 {
					numbers[idx-1].Value += number.Value
				}
				if idx < len(numbers)-2 {
					numbers[idx+2].Value += numbers[idx + 1].Value
				}
				// Check whether parent is a left or right child
				parent := number.Parent
				if parent.Parent.Left == parent {
					parent.Parent.Left = &Number{
						Parent: parent.Parent,
						Depth:  parent.Depth,
						Value:  0,
					}
				} else {
					parent.Parent.Right = &Number{
						Parent: parent.Parent,
						Depth:  parent.Depth,
						Value:  0,
					}
				}
				modified = true
				break
			}
		}
		if modified {
			continue
		}
		for _, number := range numbers {
			if number.Value > 9 {
        newNode := &Pair{
					Parent: number.Parent,
					Depth:  number.Depth,
				}
				newNode.Left = &Number{
					Parent: newNode,
					Depth:  number.Depth + 1,
					Value:  number.Value / 2,
				}
				newNode.Right = &Number{
					Parent: newNode,
					Depth:  number.Depth + 1,
					Value:  (number.Value + 1) / 2,
				}
        if number.Parent.Left == number {
          number.Parent.Left = newNode
        } else {
          number.Parent.Right = newNode
        }
        modified = true
        break
			}
		}
	}
	return p
}

type Stream struct {
	s   string
	pos int
}

func (s *Stream) Next() string {
	out := string(s.s[s.pos])
	s.pos += 1
	return out
}

func NewStream(s string) *Stream {
	return &Stream{s: s, pos: 0}
}

func NewPair(s string) *Pair {
	pair := parseValue(NewStream(s), nil, 0)
	switch v := pair.(type) {
	case *Pair:
		return v
	default:
		panic(s)
	}
}

func parseValue(s *Stream, parent *Pair, depth int) SnailNumber {
	next := s.Next()
	switch next {
	case "[":
		p := &Pair{Parent: parent, Depth: depth}
		p.Left = parseValue(s, p, depth+1)
		s.Next()
		p.Right = parseValue(s, p, depth+1)
		s.Next()
		return p
	default:
		i, _ := strconv.Atoi(next)
		return &Number{
			Parent: parent,
			Depth:  depth,
			Value:  i,
		}
	}
}

func parseInput(input string) []*Pair {
	lines := strings.Split(input, "\n")
  return parseLines(lines)
}
func parseLines(lines []string) []*Pair {
	numbers := []*Pair{}
	for i := range lines {
		numbers = append(numbers, NewPair(lines[i]))
	}
  return numbers
}

func LargestMagnitude(lines []string) int {
  largest := 0
  for i := 0; i < len(lines); i++ {
    for j := i + 1; j < len(lines); j ++ {
      m := NewPair(lines[i]).Add(NewPair(lines[j])).Magnitude()
      if m > largest {
        largest = m
      }
      m2 := NewPair(lines[j]).Add(NewPair(lines[i])).Magnitude()
      if m2 > largest {
        largest = m2
      }
    }
  }
  return largest
}

func main() {
	test_input := aoc2021lib.ReadTestInput(18)
	input := aoc2021lib.ReadInput(18)
  test_numbers := parseInput(test_input)
  numbers := parseInput(input)
  test_sum := PairSum(test_numbers)
  sum := PairSum(numbers)
  fmt.Printf("Test 1: %v\n", test_sum.Magnitude())
  fmt.Printf("Real 1: %v\n", sum.Magnitude())
  fmt.Println("Real 2: ", LargestMagnitude(strings.Split(input, "\n")))
}
